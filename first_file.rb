require 'optparse'

# create ability to provide argument
options = {}
OptionParser.new do |opts|
  opts.on("-w", "--word WORD", "the word") do |w|
    options[:word] = w
  end
end.parse!

def palidrome_test(arg)
    puts arg == arg.reverse
end

# to run on command line
# `ruby first_file.rb -w "someString"`
palidrome_test(options[:word])
